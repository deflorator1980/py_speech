import speech_recognition as sr
r = sr.Recognizer()
# harvard = sr.AudioFile('hz.wav')
# harvard = sr.AudioFile('english.wav')
# harvard = sr.AudioFile('russian.wav')
# harvard = sr.AudioFile('kings.wav')
harvard = sr.AudioFile('scor1.wav')
# harvard = sr.AudioFile('fr.wav')
with harvard as source:
    audio = r.record(source)
# print(r.recognize_google(audio))
# print(r.recognize_sphinx(audio))
# print(r.recognize_google(audio, language="fr-FR"))
text = r.recognize_google(audio, language="ru-RU")
# print(r.recognize_google(audio, language="ru-RU"))
print(text)
# f = open('example.txt','w')         # 'a' -- append
# print(text, file=f)

